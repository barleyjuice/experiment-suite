# Experiment Suite Core modules #

The main package with basic core classes for Experiment Suite applications:
 - Recorder
 - Application
 - experiments: P300, SSVEP, stimuli descriptors

## Installation ##

To install the module as dependency in your Python project simply type:
```
pip install git+https://barleyjuice@bitbucket.org/barleyjuice/experiment-suite.git#egg=es_core
```

## Use ##

### Application ###

The `es_core` package provides basic `Application` class, which allows launch and communication of all main components: `recorder` - a module for consuming data from device and log-file recording, `controller` - a module for handling stimuli configuration - types, parameters, amount, order of presentation, `presenter` - a module for present stimuli to subject according to controller configuration.

The example usage of `Application` class:

```python
# import class
from es_core import Application

# ... recorder, experimentScheme (controller) and presenter are initialized before ...
# create application instance
app = Application(presenter=presenter, experimentScheme=experimentScheme, recorder=recorder)

# run application processes
if __name__ == '__main__':
    app.run()

```

### Recorder ###

`Recorder` is an abstract class for recording signals into log files. It implements handling log files (opening, closing, renaming), event control (setup, store event flags and put them into logs) and control of the recording process (enable, disable, start, stop). 

To use the `Recorder` you must inherit it and implement abstract method `log_data` to read and put to log file data from particular device in a loop. Also you need correctly initialize it in constructor using `super().__init__(channels=..., marker_channel=...)` call. An example is presented below:

```python
# import Recorder
from es_core.recording import Recorder

# ...any another necessary imports...

# class for recording from device named Unicorn, inheriter from Recorder
class UnicornRecorder(Recorder):

    # class constructor
    def __init__(self, marker_channel) -> None:
        # create target device handler (device API wrapper)
        self.bci = Unicorn()

        # determine channel names
        self.channels = list(map(lambda ch: str(ch.name, 'utf-8'), sorted(self.bci.channels, key=lambda x: x.index)))

        # marker channel is appended to common channel list
        if marker_channel:
            self.channels.append(marker_channel)

        # init basic Recorder class
        super().__init__(channels=self.channels, marker_channel=marker_channel)

    # implement log_data function with loop of reading-writing data from Unicorn
    def log_data(self):

        # for Unicorn reading data process must be started first via API
        self.bci.startAcquisition()

        # launch endless loop - run until the recording process will be disabled
        while True:

            # stop the loop if recording process is not enabled
            if not self.enabled:
                break

            # read data from the device - 1 sample only
            data = self.bci.getData(n_samples=1)

            # save event count value to local variable (as self.event_cnt can be modified by another thread)
            current_event_cnt = self.event_cnt

            # convert data to format suit for writing in log file
            data_write = [str(sample) for (i, sample) in enumerate(data)]

            # if we use marker (event) channel, append event info
            if self.marker_channel:
                data_write.append(str(self.current_event))

                # and clear event counter because it was already recorded now
                self.clear_event(current_event_cnt)

            # finally write all collected data to the log
            self.log.write(",".join(data_write) + "\n")
        
        # if loop is ended, we stop data acquisition in Unicorn via API
        self.bci.stopAcquisition()

    # here we override non-abstract method, because we need to additionally 
    # disconnect the device on the end of the recording process
    def terminate(self):
        super().terminate()
        self.bci.closeDevice()
```

### P300Experiment ###

`P300Experiment` is a module for handling experiment with stimuli based on images presenter by odd-ball paradigm (currently we are able to present only images, but in the future the set of available stimuli types can be extended). The module controls stimuli sequence, issuing stimuli events (onset, unset) and any timings between these events - how many time each stimuli should be presented, how many time will take pause between stimuli presentation, etc. The module executes as a part of `Application` instance.

The experiment control using this module can be either *active* (when the module fully controls all the process and emits events for another modules - presenter (to present the corresponding event) and recorder (to put an event marker to the log file)) or *passive* (when the module only shares the stimuli workflow info to the presenter - amount, sequence and description of the stimuli, inter-stimuli interval etc. and then presenter handles stimuli presentation itself, corresponding the shared info). Currently `P300Experiment` is hard-coded to be *passive* due to optimization issues in coordination with the web-ui presenter.

The usage:
```python
# import module from es_core package
# ImageDesc is simple descriptor for image stimuli
from es_core.experiments import P300Experiment, ImageDesc
# ... another necessary imports ...

# create our stimuli set
stimuli = [
    ImageDesc("anger1.jpg", "anger", 3), 
    ImageDesc("anger2.jpg", "anger", 3),
    ImageDesc("happiness1.jpg", "happiness", 3),
    ImageDesc("sadness.jpg", "sadness", 6),
    ImageDesc("happiness2.jpg", "happiness", 3)
]
# ImageDesc usage:
# ImageDesc(relative_filename, category, number_or_repetitions) 

# we assume that all aforementioned stimuli are under special folder. 
# So we can use their relative name without the name of the folder
# and specify the folder name directly for presenter:
stimuli_folder = os.path.join(os.getcwd(), 'images', 'emotions')

# assume WebPresenter is already imported
presenter = WebPresenter(port=8081, stimuli_folder=stimuli_folder)

# then after completing the stimuli description we create a controller
# and pass our stimuli set
# here we use default timing parameters, 
# additional will be specified in the API 
# concerns section
experimentScheme = P300Experiment(images=stimuli)

# and finally we pass the controller to Application along with another modules 
# (assume all of the modules are created before)
app = Application(presenter=presenter, experimentScheme=experimentScheme, recorder=recorder)
```

The basic API concerns:

As a constructor, `P300Experiment(...)` takes following input parameters:
 - `images` - required - a list of ImageDesc;
 - `stimulusTime` - how many time in seconds stimulus will be presented. Default 1;
 - `interStimulusTime` - how many time in seconds the pause between stimuli will take place. Default 0.3;
 - `initialFixationTime` - how many time in seconds will be initial pause before the stimuli presentation will start. Default 2.

The workflow info can be received using function `get_workflow_meta()` after the experiment controller is constructed. (Currently this function is automatically invoked by presenter if it requests initial experiment configuration). The function takes no parameters and returns dictionary with following structure (json-friendly):
```python
{
    "sequence": list, # list of stimuli (ImageDesc items converted to dict)
    "stimulusTime": float, # how many time in seconds stimulus will be presented
    "interStimulusTime": float, # how many time in seconds the pause between stimuli will take place
    "initialFixationTime": float # how many time in seconds will be initial pause before the stimuli presentation will start
}
```

### SSVEPExperiment ###

`SSVEPExperiment` is equal to `P300Experiment`, except that stimuli are not presented by images, but by frequencies in Hz.

*Currently under development.*