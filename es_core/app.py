from multiprocessing import Process
from threading import Thread
from datetime import datetime

def is_prime(x):
    if x in [0, 1]:
        return False
    if x == 2:
        return True
    for n in range(3, int(x ** 0.5 + 1)):
        if x % n == 0:
            return False
    return True

# d is a minimal distance between generated numbers
def primes_generator(n=100, d=4):
    if hasattr(primes_generator, "D"):
        D = primes_generator.D
    else:
        primes_generator.D = D = {}

    def sieve(d):
        q = 2
        previous_q = None
        while True:
            if q not in D:
                if previous_q is None or q-previous_q >= d:
                    previous_q = q
                    yield q
                D[q * q] = [q]
            else:
                for p in D[q]:
                    D.setdefault(p + q, []).append(p)
                del D[q]

            q += 1

    return sieve(d)

class Application:
    def __init__(self, experimentScheme, *, presenter=None, recorder=None) -> None:
        self.presenter = presenter
        self.controller = experimentScheme
        self.recorder = recorder

        if self.presenter:
            self.presenter.set_application_parameters({"experiment_scheme": experimentScheme.get_scheme()})
            #self.presenter_thread = Thread(target=self.presenter.run)

        if self.recorder:
            self.recorder_thread = Thread(target=self.recorder.run)

        self.controller_thread = Thread(target=self.controller.run)

        self.session_id = "empty"
        self.session_begin_timestamp = None
        self.session_event_log = []

        self.stimuli_cat = list(set([stim["category"] for stim in self.controller.get_stimuli_set()]))
        self.stimuli_cat.sort()
        self.stimuli_event_codes = {}
        code_generator = primes_generator()
        print(self.stimuli_cat)
        for cat in self.stimuli_cat:
            code = next(code_generator)
            print(code)
            self.stimuli_event_codes[cat] = code
        
    def print_summary(self):
        summary_file = open(f"summary_{self.session_id}.txt", "w")
        summary_file.write(f"Session {self.session_id}\n")
        summary_file.write(f"Begin {self.session_begin_timestamp}, end {datetime.now()}\n\n\n")
        summary_file.write(f"Definition of event codes:\n")
        for cat in self.stimuli_cat:
            summary_file.write(f"Name: {cat}, code: {self.stimuli_event_codes[cat]}\n")
        summary_file.write("\n\n")
        summary_file.write(f"Session event log:\n")
        for evt in self.session_event_log:
            summary_file.write(f"Event: {evt['type']}, ts: {evt['time']}\n")
        summary_file.write("\n\n")
        summary_file.close()
    
    def enable_session(self):
        self.session_begin_timestamp = datetime.now()
        # self.controller.enable()
        if self.recorder:
            self.recorder.enable()

    def reset_session(self):
        self.controller.reset()
        if self.recorder:
            self.recorder.stop()
        self.session_event_log = []

    def set_id(self, id):
        self.session_id = id
        if self.recorder:
            self.recorder.set_log_id(id)

    def process_controller_event(self, event_type, stimulus_id, stimulus_cat):
        print(f"Controller event: {event_type}")
        if event_type == "end":
            self.session_event_log.append({'type': event_type, 'time': datetime.now()})
            if self.recorder:
                self.recorder.stop()
            self.print_summary()
        elif event_type == "onset":
            self.session_event_log.append({'type': stimulus_cat, 'time': datetime.now()})
            if self.recorder:
                self.recorder.set_event(self.stimuli_event_codes[stimulus_cat])
        else:
            self.session_event_log.append({'type': event_type, 'time': datetime.now()})

    def run(self):
        if self.presenter:
            self.presenter.set_start_presentation_calee(self.enable_session)
            self.presenter.set_stop_presentation_calee(self.reset_session)
            self.presenter.set_change_id_calee(self.set_id)
            self.presenter.set_event_source(self.controller)

        self.controller.on('onset', self.process_controller_event)
        self.controller.on('unset', self.process_controller_event)
        self.controller.on('end', self.process_controller_event)

        try:
            self.controller_thread.daemon=True
            if self.recorder:
                self.recorder_thread.daemon=True
                self.recorder_thread.start()

            #self.presenter_thread.daemon=True
            self.controller_thread.start()
            #self.presenter_thread.start()
            
            print("Application is running!")
            if self.presenter:
                self.presenter.run()
            # while True:
            #     time.sleep(1)
        except (KeyboardInterrupt, SystemExit):
            print ('\n! Received keyboard interrupt, quitting threads.\n')
            if self.recorder:
                self.recorder.terminate()
        