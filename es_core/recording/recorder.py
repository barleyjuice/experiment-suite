from datetime import datetime, timezone
import time
from abc import ABC, abstractmethod

def get_current_timestamp():
    return datetime.now(timezone.utc).timestamp() * 1000


class Recorder(ABC):
    default_logname = "log.csv"

    def __init__(self, channels=[], marker_channel=None) -> None:
        """constructor

        Args:
            channels (list, optional): data channel names to place in a log file header. Defaults to [].
            marker_channel (_type_, optional): name of the channel for markers (events), must be one of `channels`. Defaults to None.
        """
        self.enabled = False
        self.logname = self.default_logname
        self.log = None
        self.marker_channel = marker_channel
        self.current_event = 0
        self.event_cnt = 0
        self.channels = channels

    def run(self):
        """wait and start recording process
        """
        self.wait()

    def enable(self):
        """enable recording process
        """
        self.enabled = True

    def stop(self):
        """disable recording process
        """
        self.enabled = False

    def wait(self):
        """wait until recording will be enabled and launch it
        """
        while not self.enabled:
            time.sleep(1)
        print("Acquisition begin!")
        self.start_recording()

    def set_event(self, val=10):
        """setup new event

        Args:
            val (int, optional): event code 
            - if you use several events in one log, you should use prime numbers. Defaults to 10.
        """
        self.current_event = val
        self.event_cnt += 1
        print(f"recorder: set_event value={val}, timestamp={get_current_timestamp()}")

    def clear_event(self, last_cnt):
        """clear data of already logged event

        Args:
            last_cnt (_type_): last observed event counter value
        """
        # if values are not equal, an event was changed by another thread 
        # and we should not clear it. I think this is rather unlikely 
        # to happen, but if it will, we could handle it in such a way
        if last_cnt == self.event_cnt:
            self.current_event = 0

    def start_recording(self):
        """start recording and launch logging data loop
        """
        self.prepare_log_file()
        self.event_cnt = 0
        self.log_data()

    @abstractmethod
    def log_data(self):
        pass

    def prepare_log_file(self):
        """open and prepare current log file
        """
        header = ",".join(self.channels) + "\n"
        self.log = open(self.logname, "w")
        self.log.write(header)

    def set_log_id(self, id):
        """set new log name

        Args:
            id (_type_): unique string in log file name
        """
        self.logname = f"log_{id}.csv"

    def terminate(self):
        """graceful stop of the recording process
        """
        self.stop()
        if self.log:
            self.log.close()