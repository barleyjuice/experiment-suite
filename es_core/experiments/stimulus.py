import collections
import uuid

class Stimulus:
    def __init__(self, category) -> None:
        self.category = category
        self.id = uuid.uuid4()

    def asdict(self):
        return {'category': self.category, 'id': self.id}

    def __repr__(self):
        return f"<Stimulus category={self.category}"

class ImageStimulus(Stimulus):
    def __init__(self, imagePath, category) -> None:
        self.imageSrc = imagePath
        super().__init__(category)

    def asdict(self):
        return {'src': self.imageSrc, 'category': self.category, 'id': self.id}

    def __repr__(self):
        return f"<ImageStimulus src={self.imageSrc}, category={self.category}"

class FrequencyStimulus(Stimulus):
    def __init__(self, frequency, category) -> None:
        self.frequency = frequency
        super().__init__(category)

    def asdict(self):
        return {'frequency': self.frequency, 'category': self.category, 'id': self.id}

    def __repr__(self):
        return f"<FrequencyStimulus value={self.frequency}, category={self.category}"

ImageDesc = collections.namedtuple("ImageDesc", ("src", "category", "repetition"))