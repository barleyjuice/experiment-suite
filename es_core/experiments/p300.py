from .experiment import Experiment
from .present_params import SelectionMode, Order
from .stimulus import ImageStimulus

class P300Experiment(Experiment):
    def __init__(self, images, mode=SelectionMode.SWITCH, order=Order.RANDOM, stimulusTime=1, interStimulusTime=0.3, initialFixationTime=2) -> None:
        self.imageSources = images

        imageStimuli = list(map(lambda image: [ImageStimulus(image.src, image.category)]*image.repetition, images))

        super().__init__(stimuli=imageStimuli, order=order, mode=mode, \
            stimulusTime=stimulusTime, interStimulusTime=interStimulusTime, initialFixationTime=initialFixationTime)

    def get_scheme(self):
        return f"P300-{super().get_scheme()}"