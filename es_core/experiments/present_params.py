from enum import Enum

class Order(Enum):
    RANDOM = 1,
    SEQUIENTIAL = 2

class SelectionMode(Enum):
    HIGHLIGHT_SINGLE = 1,
    HIGHLIGHT_GROUP = 2,
    SWITCH = 3,
    NONE = 0