import time
from datetime import datetime, timezone
import threading
from .present_params import Order
from random import shuffle

def get_current_timestamp():
    return datetime.now(timezone.utc).timestamp() * 1000

def create_stim_sequence(stimuli, order):
    unpacked_stim_list = []
    for stimulus_arr in stimuli:
        unpacked_stim_list = [*unpacked_stim_list, *stimulus_arr]
    if order is Order.SEQUIENTIAL:
        return unpacked_stim_list
    elif order is Order.RANDOM:
        shuffle(unpacked_stim_list)
        return unpacked_stim_list
    else:
        raise RuntimeError(f"Unexpected stimuli order param: {order}")

def create_sequence_generator(seq):
    for item in seq:
        yield item

class Experiment:
    def __init__(self, *, stimuli, order, mode, stimulusTime, interStimulusTime, initialFixationTime) -> None:
        self.stimuli = stimuli
        self.order = order
        self.mode = mode
        self.stimulusTime = stimulusTime
        self.interStimulusTime = interStimulusTime
        self.initialFixationTime = initialFixationTime

        self.stimuli_sequence = create_stim_sequence(stimuli=stimuli, order=order)
        self.stimuli_generator = None

        self.timer_set = None
        self.timer_unset = None

        self.current_stimuli = None

        self.enabled = False
        self.running = False

        self.listeners = {
            "onset": [],
            "unset": [],
            "end": []
        }

    def shuffle_stimuli(self):
        self.stimuli_sequence = create_stim_sequence(stimuli=self.stimuli, order=self.order)

    def enable(self):
        self.enabled = True

    def reset(self):
        self.enabled = False

    def get_stimuli_set(self):
        return list(map(lambda st_obj: st_obj.asdict(), set(self.stimuli_sequence)))

    def get_workflow_meta(self):
        return {
            "sequence": list(map(lambda st_obj: st_obj.asdict(), self.stimuli_sequence)),
            "stimulusTime": self.stimulusTime,
            "interStimulusTime": self.interStimulusTime,
            "initialFixationTime": self.initialFixationTime
        }

    def get_scheme(self):
        return "random" if self.order is Order.RANDOM \
            else "sequiential" if self.order is Order.SEQUIENTIAL \
                else "unknown"

    def on(self, event_type, listener):
        self.listeners[event_type].append(listener)

    def emit(self, event, id=None, category=None):
        for listener in self.listeners[event]:
            listener(event, id, category)

    def _start_next_stimuli_timer(self):
        self.timer_set = threading.Timer(self.interStimulusTime, self._set_next_stimuli).start()
    
    def _start_unset_stimuli_timer(self):
        self.timer_unset = threading.Timer(self.stimulusTime, self._unset_stimuli).start()
    
    def run(self):
        self.shuffle_stimuli()
        self.stimuli_generator = create_sequence_generator(self.stimuli_sequence)
        while not self.enabled:
            time.sleep(1)
        print("Starting experiment!")
        self._set_next_stimuli()

    def emit_onset(self, id, category):
        self.emit("onset", id, category)

    def emit_unset(self):
        self.emit("unset")

    def emit_end(self):
        self.emit("end")

    def _set_next_stimuli(self):
        if not self.enabled:
            self._stop()
            return
        try:
            self.current_stimuli = next(self.stimuli_generator)
            print(f"stimuli {self.current_stimuli.category} was set!")
            self.emit_onset(self.current_stimuli.id, self.current_stimuli.category)
            print(f"controller: set_event value={self.current_stimuli.category}, timestamp={get_current_timestamp()}")
            self._start_unset_stimuli_timer()
        except StopIteration:
            self._stop()

    def _unset_stimuli(self):
        if not self.enabled:
            self._stop()
            return
        self.current_stimuli = None
        print("unset!")
        self.emit_unset()
        self._start_next_stimuli_timer()    

    def _stop(self):
        self.current_stimuli = None
        self.enabled = False
        print("end of the session!")
        self.emit_end()
        self.run()