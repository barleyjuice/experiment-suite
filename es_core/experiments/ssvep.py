from .experiment import Experiment
from .present_params import SelectionMode, Order
from .stimulus import FrequencyStimulus

class SSVEPExperiment(Experiment):
    def __init__(self, frequencies, mode=SelectionMode.SWITCH, order=Order.RANDOM, stimulusTime=1, interStimulusTime=0.3, initialFixationTime=2) -> None:
        self.frequencies = frequencies

        freqStimuli = list(map(lambda freq: [FrequencyStimulus(freq.value, freq.category)]*freq.repetition, frequencies))        

        super().__init__(stimuli=freqStimuli, order=order, mode=mode, \
            stimulusTime=stimulusTime, interStimulusTime=interStimulusTime, initialFixationTime=initialFixationTime)

    def get_scheme(self):
        return f"SSVEP-{super().get_scheme()}"