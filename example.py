from multiprocessing.spawn import freeze_support
from es_core.experiments import P300Experiment, ImageDesc
from es_recording.unicorn import UnicornRecorder
from es_presentation.web import WebPresenter
from es_core import Application

import os

stimuli = [
    ImageDesc("anger1.jpg", "anger", 1), 
    ImageDesc("anger2.jpg", "anger", 1),
    ImageDesc("happiness1.jpg", "happiness", 2),
    ImageDesc("sadness.jpg", "sadness", 2),
    ImageDesc("happiness2.jpg", "happiness", 2)
]

stimuli_folder = os.path.join(os.getcwd(), 'images', 'emotions')
presenter = WebPresenter(port=8081, stimuli_folder=stimuli_folder)
experimentScheme = P300Experiment(images=stimuli)
recorder = UnicornRecorder(marker_channel="EVENT_MARKER")
app = Application(presenter=presenter, experimentScheme=experimentScheme, recorder=recorder)

if __name__ == '__main__':
    freeze_support()
    app.run()

