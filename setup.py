from distutils.extension import Extension
from setuptools import setup, find_packages

setup(
    name='es_core',
    version='0.1.0',
    author='Aliona Petrova',
    author_email='consciencee95@gmail.com',
    packages=find_packages(),
    url='https://bitbucket.org/barleyjuice/experiment-suite/',
    description='Experiment Suite Core modules (basic core classes)',
    long_description=open('README.md').read()
)
