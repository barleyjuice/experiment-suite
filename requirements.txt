bidict==0.21.2
cffi==1.14.5
click==8.0.1
colorama==0.4.4
es-presentation-web @ git+https://bitbucket.org/barleyjuice/es_presentation_web.git@296c45f9cc28b4fe9dc82ded5b9c44475b81e995
Flask==1.1.2
Flask-SocketIO==5.1.0
gevent==21.1.2
gevent-websocket==0.10.1
greenlet==1.1.0
itsdangerous==2.0.1
Jinja2==3.0.1
MarkupSafe==2.0.1
pycparser==2.20
python-engineio==4.2.0
python-socketio==5.3.0
redis==3.5.3
six==1.16.0
unicorn-api @ git+https://bitbucket.org/barleyjuice/unicorn-api.git@bed22995e117f20a747e28f48b5e068c9c8e18cb
Werkzeug==2.0.1
zope.event==4.5.0
zope.interface==5.4.0
